<?php


namespace Point\Customer\Block\Cart;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\ResourceConnection;


class History extends \Magento\Framework\View\Element\Template
{
    const MAIN_TABLE = 'reward_point_history';
    /**
     * @var Template\Context
     */
    private $context;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;
    /**
     * @var \Point\Customer\Model\History
     */
    private $history;
    /**
     * @var array
     */
    private $data;


    private $connection;


    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Point\Customer\Model\History $history,
        ResourceConnection $connection,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->context = $context;
        $this->customerSession = $customerSession;
        $this->history = $history;
        $this->connection = $connection;
        $this->data = $data;
    }

    public function getHistory()
    {

     /*   $connect = $this->connection->getConnection();
        $select = $connect->select()
            ->from(['rw' => self::MAIN_TABLE], ['*']);*/
/*        ->where('id = ?', 10)*/
/*        ->group('id');*/

        /*echo "<pre>";
        print_r($select->query()-> fetchAll());
        die();

        return $select->query()->fetchAll();*/

        $customerId= $this->customerSession->getCustomer()->getId();
        return $this->history->getCollection()->addFieldToFilter('customer_id', $customerId)->getData();
    }
}
