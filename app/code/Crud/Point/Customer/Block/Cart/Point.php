<?php


namespace Point\Customer\Block\Cart;

use Magento\Customer\Api\CustomerRepositoryInterface;

class Point extends \Magento\Framework\View\Element\Template
{
    protected $_urlBuilder;
    protected $customerRepositoryInterface;
    protected $customerSession;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    )
    {
        parent::__construct($context, $data);
/*        $this->customerRepositoryInterface= $customerRepositoryInterface;*/
        $this->customerSession = $customerSession;

        $this->_construct();
    }

    public function getPoint(){
        $currenPoint = $this->customerSession->getCustomer()->getData('point');
        return $currenPoint;
        /*$customerId= $this->customerSession->getCustomer()->getId();
        $customer = $this->customerRepositoryInterface->getById($customerId);
        $customerAttributeData = $customer->__toArray();
        $currentPoint = $customerAttributeData['custom_attributes']['point']['value'] ;
        return $currentPoint;*/
    }
}
