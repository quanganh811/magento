<?php


namespace Point\Customer\Block\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CustomDiscount extends \Magento\Framework\View\Element\Template
{
    protected $quoteResourcemodel;
    protected $quote;
    protected $apply;
    protected $customerSession;
    protected $scopeConfig;
    protected $cart;
    protected $customerRepositoryInterface;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Point\Customer\Model\Quote $quote,
        \Point\Customer\Model\ResourceModel\Quote $quoteResourcemodel,
        \Magento\Customer\Model\Session $customerSession,
        ScopeConfigInterface  $scopeConfig,
        \Magento\Checkout\Model\Cart $cart,
        CustomerRepositoryInterface $customerRepositoryInterface,
        array $data = []
    )
    {
        $this->cart = $cart;
        $this->scopeConfig = $scopeConfig;
        $this->quoteResourcemodel = $quoteResourcemodel;
        $this->customerSession = $customerSession;
        $this->quote = $quote;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        parent::__construct($context, $data);
        $this->_construct();
    }

    public function getCustomerId(){
        return $this->customerSession->getCustomer()->getId();
    }
    public function getPointApply(){
        $data = $this->quoteResourcemodel->getByCustomerId($this->getCustomerId());
        return $data['reward_points'] ;
    }
    public function getPoint(){
/*        return $this->customerSession->getCustomer()->getData('point');*/

        $customer = $this->customerRepositoryInterface->getById($this->getCustomerId());
        $customerAttributeData = $customer->__toArray();
        return $customerAttributeData['custom_attributes']['point']['value'] ;
    }
    public function getDiscount(): array
    {
        $pointapply = $this->getPointapply();
        $discount = $pointapply * 10;
        //$subTotal = $this->cart->getQuote()->getSubtotal();
        $point = $this->getPoint();
        $aryDiscount['pointapply'] = $pointapply;
        $aryDiscount['discount'] = $discount;
        $aryDiscount['point'] = $point;
        return $aryDiscount;
    }

}
