<?php


namespace Point\Customer\Model;
use Magento\Framework\Model\AbstractModel;

class History extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Point\Customer\Model\ResourceModel\History');
    }
    public function addHistoryEarn($customerId, $point )
    {
            $this->addData([
                'customer_id' => $customerId,
                'points' => $point,
                'action' => false,
                'date'=> time()
            ]);
            $this->save();
        }
    public function addHistoryRedeem($customerId, $point )
    {
        $this->addData([
            'customer_id' => $customerId,
            'points' => $point,
            'action' => true,
            'date'=> time()
        ]);
        $this->save();
    }
}
