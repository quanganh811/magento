<?php

namespace Point\Customer\Model;

use Magento\Framework\Model\AbstractModel;

class Quote extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Point\Customer\Model\ResourceModel\Quote');
    }

//    /**
//     * @param $customerId
//     * @param $amount
//     * @deprecated since 1.12.0
//     */
    public function addReward($customerId, $amount)
    {
        $customer =  $this->getResource()->getByCustomerId($customerId);
        if (!$customer) {
            $this->addData([
                'customer_id'   => $customerId,
                'reward_points' => $amount
            ]);
            $this->save();
        } else {
            $this->addData([
                'id'            => $customer['id'],
                'customer_id'   => $customerId,
                'reward_points' => $amount

            ]);
            $this->save();
        }
    }

}
