<?php


namespace Point\Customer\Model\ResourceModel;


class History extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize connection and define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('reward_point_history', 'id');
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCustomerId($customerId)
    {
        $select = $this->getConnection()->select()->from($this->getMainTable())->where('customer_id=:customer_id');
        $result = $this->getConnection()->fetchRow(
            $select,
            [
                'customer_id' => $customerId
            ]
        );

        if (!$result) {
            return [];
        }

        return $result;
    }
}
