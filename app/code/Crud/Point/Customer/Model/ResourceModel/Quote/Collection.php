<?php

namespace Point\Customer\Model\ResourceModel\Quote;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Point\Customer\Model\Quote::class, \Point\Customer\Model\ResourceModel\Quote::class);
    }
}
