<?php

namespace Crud\Banner\Observer;

use Magento\Framework\Event\Observer;

class ChangeName implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(Observer $observer)
    {
        $data = $observer->getData('postData');
        $data->setData('link', 'room.com');
        $observer->setData('postData', $data);
    }
}
