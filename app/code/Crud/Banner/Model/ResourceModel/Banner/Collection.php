<?php

namespace Crud\Banner\Model\ResourceModel\Banner;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Crud\Banner\Model\Banner', 'Crud\Banner\Model\ResourceModel\Banner');
    }
}

