<?php

namespace Crud\Banner\Model;

class Banner extends \Magento\Framework\Model\AbstractModel
{

    protected function _construct()
    {
        $this->_init('Crud\Banner\Model\ResourceModel\Banner');
    }
}
