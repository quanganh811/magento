<?php

namespace Crud\Banner\Controller\Adminhtml\Index;
class Index extends \Magento\Backend\App\Action
{


    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        // Load layout and set active menu
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Crud_Banner::banner_manager');
        $resultPage->getConfig()->getTitle()->prepend(__('Test manager'));

        return $resultPage;
    }

}
