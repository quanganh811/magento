<?php
namespace Crud\Banner\Controller\Adminhtml\Index;

use Crud\Banner\Model\BannerFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package ViMagento\HelloWorld\Controller\Adminhtml\banner
 */
class Save extends Action
{
    /**
     * @var bannerFactory
     */
    private $bannerFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param bannerFactory $bannerFactory
     */
    public function __construct(
        Action\Context $context,
        bannerFactory $bannerFactory
    ) {
        parent::__construct($context);
        $this->bannerFactory = $bannerFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getpostValue();
        $id = !empty($data['id']) ? $data['id'] : null;

        $newData = [
            'images' => $data['images'],
            'link' => $data['link'],
            'status' => $data['status'],
        ];

        $banner = $this->bannerFactory->create();


        if ($id) {
            $banner->load($id);
        }
        $data['image']= $data['images'][0]['name'];
        try {
            $banner->addData($newData);
            $banner->save();
            $this->messageManager->addSuccessMessage(__('You saved the banner.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }




        return $this->resultRedirectFactory->create()->setPath('banner/index/index');
    }


}
