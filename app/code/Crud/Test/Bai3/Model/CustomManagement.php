<?php

namespace Test\Bai3\Model;

/**
 * Class CustomManagement
 * @package Test\Bai3\Model
 */
class CustomManagement implements \Test\Bai3\Api\CustomManagementInterface
{
    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        try{
            $response = [
                'name' => 'long',
                'age' => '25',
                'job' => 'developer'
            ];
        } catch (\Exception $e) {
            $response=['error' => $e->getMessage()];
        }
        return json_encode($response);
    }
}
