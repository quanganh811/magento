<?php

namespace Test\Bai1\Plugins;

class  Cart
{
    public function beforeAddProduct(\Magento\Checkout\Model\Cart $subject, $productInfo, $requestInfo = null)
    {
        try {
            $requestInfo['qty']=5;
            return array($productInfo, $requestInfo);
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
        return [$productInfo, $requestInfo];
    }

    /*public function aroundAddProduct(\Magento\Checkout\Model\Cart $subject, callable $proceed, $productInfo, $requestInfo = null)
    {
        $requestInfo['qty']=10;
        $result= $proceed($productInfo, $requestInfo);
        return $result;
    }*/
}



