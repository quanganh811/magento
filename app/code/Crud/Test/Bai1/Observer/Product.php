<?php
namespace Test\Bai1\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Block\Product\ListProduct;
class Product implements ObserverInterface{
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $collection= $observer->getEvent()->getData('collection');
/*        var_dump($collection);die();*/
        foreach ($collection as $product){
            $price = $product->getData('price');
            $name = $product->getData('name');
            if($price < 200){
                $name .= " Cheap";
            }
            else{
                $name .= " Expensive";
            }
            $product->setData('name',$name);
/*            $product->setData('price',100);*/
        }

    }
}
