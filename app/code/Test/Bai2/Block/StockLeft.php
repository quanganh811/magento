<?php
namespace  Test\Bai2\Block;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\StockRegistry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;

/**
 *
 */
class StockLeft extends Template{
    private $registry;
    private $stockRegistry;
    private $customerSession;
    /**
     * @param Template\Context $context
     * @param Registry $registry
     * @param StockRegistryInterface $stockRegistry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        StockRegistryInterface $stockRegistry,
        \Magento\Customer\Model\Session $customerSession,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->registry = $registry;
        $this->stockRegistry = $stockRegistry;
        $this->customerSession= $customerSession;
    }

    public function getDate(){
        return date("d:m:y");
    }
    public function getItem(){
        $product=$this->getCurrentProduct();
        $stock=$this->stockRegistry->getStockItem($product->getId());
        return $stock->getQty();
    }
    protected function getCurrentProduct(){
        return $this->registry->registry('product');
    }
    public function getCustomerId(){
    /*        var_dump($this->customerSession->getCustomer());die();*/
        return $this->customerSession->getCustomer()->getName();
    }
}
