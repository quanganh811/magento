<?php

namespace Test\Bai3\Api;

/**
 * Interface CustomManagementInterface
 * @package Test\Bai3\Api
 */
interface CustomManagementInterface
{
    /**
     * Get list custom
     * @return mixed
     */
    public function getList();
}
