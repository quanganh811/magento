<?php

namespace Point\Customer\Model\ResourceModel;
class Quote extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize connection and define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('reward_point_quote', 'id');
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCustomerId($customerId)
    {
        $select = $this->getConnection()->select()->from($this->getMainTable())->where('customer_id=:customer_id');

        $result = $this->getConnection()->fetchRow(
            $select,
            [
                'customer_id' => $customerId
            ]
        );
        if (!$result) {
            return [];
        }
        return $result;
    }


    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    /*public function getPointapply($customerId){
        $data= $this->getByCustomerId($customerId);
        return $data['reward_points'];
    }*/

    public function getPointapply($customerId){
        $data= $this->getPointapply($customerId);
        return $data['reward_point'];
    }
}
