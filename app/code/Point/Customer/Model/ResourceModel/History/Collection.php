<?php


namespace Point\Customer\Model\ResourceModel\History;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Point\Customer\Model\History::class,
            \Point\Customer\Model\ResourceModel\History::class);
    }
}
