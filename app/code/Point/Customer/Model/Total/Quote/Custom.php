<?php


namespace Point\Customer\Model\Total\Quote;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Point\Customer\Api\CustomManagementInterface;

/**
 * Class Custom
 * @package Mageplaza\HelloWorld\Model\Total\Quote
 */
class Custom extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal implements CustomManagementInterface
{
    protected $customerSession;
    protected $scopeConfig;
    protected $quoteResourcemodel;
    protected $quote;
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;
    /**
     * Custom constructor.
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Point\Customer\Model\Quote $quote,
        \Point\Customer\Model\ResourceModel\Quote $quoteResourcemodel,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\App\Config\ScopeConfigInterface  $scopeConfig
    ){
        $this->customerSession = $customerSession;
        $this->quoteResourcemodel=$quoteResourcemodel;
        $this->quote=$quote;
        $this->_priceCurrency = $priceCurrency;
        $this->scopeConfig =$scopeConfig;
    }

    public function getCustomerId(){
        $customerID = $this->customerSession->getCustomer()->getId();
        return $customerID;
    }
    public function getPoint(){
        $pointCurrent= $this->customerSession->getCustomer()->getData('point');
        return $pointCurrent;
    }
    public function getDiscount(){
        $data= $this->quoteResourcemodel->getByCustomerId($this->getCustomerId());
        $pointApply=$data['reward_points'];
        if($this->getPoint() >= $pointApply){
            return $pointApply * 10;
        }else{
            return 0;
        }
    }
    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this|bool
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);
        $baseDiscount = $this->getDiscount();
        $discount =  $this->_priceCurrency->convert($baseDiscount);
        $total->addTotalAmount('customdiscount', -$discount);
        $total->addBaseTotalAmount('customdiscount', -$baseDiscount);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $baseDiscount);
        $quote->setCustomDiscount(-$discount);
        return $this;
    }

    /**
     * {@inheritdoc}
     */

    public function getList()
    {
      /*  $CustomerId = $this->getCustomerId();
        $Discount = $this->getDiscount();*/
        try{
            $response = [
                'id'=> 2,
                'discount' => 1000
            ];
        } catch (\Exception $e) {
            $response=['error' => $e->getMessage()];
        }
        return json_encode($response);
    }
}
