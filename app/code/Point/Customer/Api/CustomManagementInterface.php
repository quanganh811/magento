<?php

namespace Point\Customer\Api;

/**
 * Interface CustomManagementInterface
 * @package Point\Customer\Api
 */
interface CustomManagementInterface
{
    /**
     * Get list custom
     * @return mixed
     */
    public function getList();
}
