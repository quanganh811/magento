<?php
namespace Point\Customer\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class OrderdataObserver implements ObserverInterface
{
    const ENABLED ="setting/point/enabled";
    protected $logger;
    protected $customer;
    protected $customerRepositoryInterface;
    protected $scopeConfig;
    protected $quoteResourcemodel;
    protected $quote;
    protected $historyResourcemodel;
    protected $historyFactory;
    const POINT = "point";
    public function __construct(LoggerInterface $logger,
                                \Point\Customer\Model\ResourceModel\Quote $quoteResourcemodel,
                                \Point\Customer\Model\Quote $quote,
                                Customer $customer,
                                CustomerRepositoryInterface $customerRepositoryInterface,
                                ScopeConfigInterface  $scopeConfig,
                                \Point\Customer\Model\ResourceModel\History $historyResourcemodel,
                                \Point\Customer\Model\HistoryFactory $historyFactory
    )
    {
        $this->historyFactory=$historyFactory;
        $this->historyResourcemodel=$historyResourcemodel;
        $this->quote=$quote;
        $this->quoteResourcemodel=$quoteResourcemodel;
        $this->scopeConfig =$scopeConfig;
        $this->customer = $customer;
        $this->customerRepositoryInterface= $customerRepositoryInterface;
        $this->logger = $logger;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function execute(Observer $observer)
    {
            //get id order customer
             $customerId = $observer->getEvent()->getOrder()-> getCustomerId();

             //get Subtotal
            $orderSubtotal =  $observer->getEvent()->getOrder()->getData('base_subtotal');

            // get currentPoint
            $customer = $this->customerRepositoryInterface->getById($customerId);
            $customerAttributeData = $customer->__toArray();
            $currentPoint = $customerAttributeData['custom_attributes']['point']['value'];

            //get Point after order
            $point_earn = $orderSubtotal /100;

            //add point in history
            $this->historyFactory->create()->addHistoryEarn($customerId, $point_earn);

            // point_redeem
            $post = $this->quoteResourcemodel->getByCustomerId($customerId);
            $point_redeem = $post['reward_points'];

            //add point -> history
            $this->historyFactory->create()->addHistoryRedeem($customerId, $point_redeem);

            //get point after order
            $last_point = $currentPoint + $point_earn - $point_redeem;

            //save
             $customer->setCustomAttribute('point', $last_point);
             $this->customerRepositoryInterface->save($customer);

             //remove point in quote
             $post = $this->quoteResourcemodel->getByCustomerId($customerId);
             $id = $post['id'];
             $this->quote->load($id)->setData('reward_points', 0)->save();
    }
}
