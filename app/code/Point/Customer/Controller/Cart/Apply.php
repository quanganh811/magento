<?php

namespace Point\Customer\Controller\Cart;

use Magento\Backend\App\Action;
use Magento\Checkout\Controller\Cart;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Controller\ResultFactory;


class Apply extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    protected $quote;
    protected $quoteResourcemodel;
    protected $customerSession;
    protected $customerRepositoryInterface;
    protected $messageManager;
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Point\Customer\Model\ResourceModel\Quote $quoteResourcemodel,
        \Point\Customer\Model\Quote $quote,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->quote=$quote;
        $this->quoteResourcemodel=$quoteResourcemodel;
        $this->customerRepositoryInterface= $customerRepositoryInterface;
        $this->customerSession = $customerSession;
        parent::__construct($context);
        $this->messageManager = $messageManager;
    }
    /*public function getapplyPoint(){
        return $this->getRequest()->getParam('point');
    }*/

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        //get points from form
        $point_apply= $this->getRequest()->getParam('point');
        //$point_apply= $this->getapplyPoint();
        //get id customer
        $customerId= $this->customerSession->getCustomer()->getId();
        $currentPoint= $this->customerSession->getCustomer()->getData('point');
        //get point
        /*$customer = $this->customerRepositoryInterface->getById($customerId);
        $customerAttributeData = $customer->__toArray();
        $currentPoint= $customerAttributeData['custom_attributes']['point']['value'];*/

        //save point
        if($currentPoint >= $point_apply &&  $point_apply != 0 && $point_apply > 0){
            $post = $this->quoteResourcemodel->getByCustomerId($customerId);
            $id = $post['id'];
            $this->quote->load($id)->setData('reward_points', $point_apply)->save();
            $this->messageManager->addSuccess('You used '.$point_apply.' points');
        }else{
            if($point_apply == 0){
                $this->messageManager->addError('Please apply point other than 0');
            }else{
                $this->messageManager->addError('You don\'t have enough points ');
            }

        }


        // return url
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }
}
