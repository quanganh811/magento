var config = {
    paths: {
        'magepow/elevatezoom'	: 'Zoom_Product/js/jquery.elevatezoom',
    },
    shim: {
        'magepow/elevatezoom': {
            deps: ['jquery']
        }
    }
};
